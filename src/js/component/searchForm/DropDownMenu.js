import React from 'react';
import DropDownOption from './DropDownOption'

class DropDownMenu extends React.Component {
  render () {
    return (
      <select className="search-form__dropdown background background__grey">
        {this.props.list.map(function(result) {
            return <DropDownOption key={result.id} value={result.text}/>;
        })}
      </select>
    );
  }
}

export default DropDownMenu;
