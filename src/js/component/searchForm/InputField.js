import React from 'react';

class InputField extends React.Component {
  render () {
    return (
      <input type={this.props.type} placeholder={this.props.placeholder} className={this.props.classes} />
    );
  }
}

export default InputField;
