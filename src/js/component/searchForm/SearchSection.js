import React from 'react';
import ToggleList from './toggleList';
import SearchForms from './SearchForms';
import searchFormData from '../../data/searchFormData';

class SearchSection extends React.Component {

  constructor(props) {
    super(props);
    this.state = {active : 1};
    this._onGetiTemSelected = this._onGetiTemSelected.bind(this);
  }

   _onGetiTemSelected (item) {
     this.setState({ active : item });
   }

  render () {
    return (
      <div>
        <ToggleList list={searchFormData.formToggleButton} onGetiTemSelected={this._onGetiTemSelected} status={this.state.active}/>
        <SearchForms list={searchFormData.formDropDown1} form={1} status={this.state.active}/>
        <SearchForms list={searchFormData.formDropDown2} form={2} status={this.state.active}/>
        <SearchForms list={searchFormData.formDropDown3} form={3} status={this.state.active}/>
      </div>
    );
  }

}

export default SearchSection;
