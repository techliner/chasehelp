import React from 'react';
import classNames from 'classnames';


class ToggleListItem extends React.Component {

  render() {
    let btnClass = classNames({
      'button uppercase': true,
      'active': this.props.status === this.props.data.id
    });

    return (
      <li className={btnClass} onClick={this.props.action}>{this.props.data.text}</li>
    );
  }

}

export default ToggleListItem
