import React from 'react';
import classNames from 'classnames';
import InputField from './InputField';
import DropDownMenu from './DropDownMenu';


class SearchForms extends React.Component {

  render () {

    let formClass = classNames({
      'search-form flush--top': true,
      'show': this.props.status === this.props.form
    });

    return (
            <form className={formClass}>
              <DropDownMenu list={this.props.list} />

              <InputField
                type="text"
                placeholder="Search"
                classes="search__field background background__grey push--left"/>
              <button className="btn btn--small btn--hard background background__base one-tenth uppercase font-base-light push--left">Search</button>
            </form>
    );
  }
}

export default SearchForms
