import React from 'react';
import ToggleListItem from './ToggleListItem';

class ToggleList extends React.Component {

  render () {
    let that = this;
    return (
        <ul className="nav form-toggle">
            {this.props.list.map(function(result) {
                return <ToggleListItem
                        key={result.id}
                        data={result}
                        action={that.props.onGetiTemSelected.bind(null,result.id)}
                        status = {that.props.status}
                        />;
            })}
        </ul>
    );
  }
}

export default ToggleList;
