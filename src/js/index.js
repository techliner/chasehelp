import React from 'react';
import {render} from 'react-dom';
import SearchSection from './component/searchForm/SearchSection';

render(
    <SearchSection />,
    document.getElementById('search-form')
);
