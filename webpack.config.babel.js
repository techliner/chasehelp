import path from 'path';

const BUILD_DIR = path.resolve(__dirname, 'html/js'),
      APP_DIR = path.resolve(__dirname, 'src/js');

export default {
  entry: APP_DIR + '/index.js',
  output: {
    path: BUILD_DIR,
    filename: 'bundle.js'
  },
  module : {
    loaders : [
      {
        test : /\.jsx?/,
        include : APP_DIR,
        loader : 'babel'
      }
    ]
  }
};
