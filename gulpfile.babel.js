import gulp from 'gulp';
import sass from 'gulp-sass';
import watch from 'gulp-watch';
import webpack from 'webpack-stream';
import webpackConfig from './webpack.config.babel';

/* Environment Path */
const path = {
    public: "html/",
    src: "src/",
    sass () { return this.src + "scss/" },
    js () { return this.src + "js/" },
    BuildJs () { return this.public + "js/" },
    buildCss () { return this.public + "css/" }
};

/**
 * webpack - react es6 compilation
 */
gulp.task('webpack', () => {
  return gulp.src(path.js()+'index.js')
    .pipe(webpack( webpackConfig ))
    .pipe(gulp.dest(path.BuildJs()));
});

/**
 * SASS compilation
 */
gulp.task('sass', () => {
    return gulp.src(path.sass() + '*.scss')
        .pipe(sass({
            outputStyle: 'compressed',
            loadPath: __dirname + path.src
        }))
        .pipe(gulp.dest(path.buildCss()));
});

/**
 * Spying on changes
 */
gulp.task('spy', () =>  {
    gulp.watch( path.sass() + '**/*.scss', ['sass']);
    gulp.watch( path.js() + '**/*.js', ['webpack']);
});


// Default Task
gulp.task('default', ['spy']);
