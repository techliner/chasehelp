<?php include 'global/head.php' ?>
<?php include 'global/header.php' ?>
<main>
    <section class="background background__grey-light soft--bottom">
        <div class="container soft--top">
            <h1 class="font-base soft--top">Why Chastehelp</h1>

            <h3 class="font-brand">WAYS TO GIVE</h3>
            <p>ChasteHelp makes giving simple, safe and trustworthy. We are a one-stop shop for charitable giving and fundraising. We act as an e-meeting point for non-profit organizations, corporations, freelancers and citizens who want to support or be supported by the initiations we develop on our platform. In this section, please explore all the ways you can give through ChasteHelp!</p>
            <ul class="nav soft--bottom">
              <li><a href="" class="font-base font-fredoka push-half--right">Donate for a project</a></li>
              <li><a href="" class="font-fredoka font-brand">Donate to a Charity</a></li>
            </ul>

            <h3 class="font-brand">NO FEES</h3>
            <p>ChasteHelp is a non-profit organisation. Our mission is to increase charitable giving and it is critical that we pass along all the money we collect. Apart from the PayPal administrative costs all the money you donate through ChasteHelp goes directly to our Field Partners. ChasteHelp does not take a cut. Furthermore, we do not charge any interest to our Field Partners either.</p>
            <p>As a newcomer, ChasteHelp is exclusively funded through the optional donations that the users of the platform make when they are supporting our initiations uploaded, or when we work together building and designing new initiations to be supported.</p>
            <p>We are incredibly thankful for the support that has enabled us to do the work that we hope is going to touch the lives of so many people.</p>
            <p>Learn more about ways to give :</p>
            <ul class="nav soft--bottom">
              <li><a href="" class="font-base font-fredoka push-half--right">Donate for A Project  Donate to A Charity or How to support us</a></li>
            </ul>

            <h3 class="font-brand">FOR DONORS</h3>
            <p>ChasteHelp is primarily inspired from us, the citizens ourselves, and our need to redefine our connection to the civil society as well as, our rethinking of how we give. Whether you want to give once or on a more regular basis with ChasteHelp you can donate online easily and safely. You can even give in celebration, in tribute on in memory. By the end of the donation, you will always receive a charitable tax receipt.</p>
            <ul class="nav soft--bottom">
              <li><a href="" class="font-base font-fredoka push-half--right">Donate for a project</a></li>
              <li><a href="" class="font-fredoka font-brand push-half--right">Gift Card</a></li>
              <li><a href="" class="font-base font-fredoka">Scheduled Giving</a></li>
            </ul>

            <h3 class="font-brand">FOR CHARITIES</h3>
            <p>ChasteHelp is your trusted partner for collecting donations online. Along with our Field Partners on the focus areas that ChasteHelp takes action on, we choose and we form all the initiatives uploaded on our platform, in order to get financed by the donors.  In addition, quickly and easily, charities can accept donations directly through ChasteHelp letting their donors decide on their donations' both schedule and form.</p>
            <ul class="nav soft--bottom">
              <li><a href="" class="font-base font-fredoka push-half--right">Propose A Project to Fund</a></li>
              <li><a href="" class="font-fredoka font-brand push-half--right">Donate to A Charity</a></li>
            </ul>

            <h3 class="font-brand">FOR CORPORATIONS</h3>
            <p>Doing social good is a powerful way to enhance your brand and motivate and connect with employees and consumers. At ChasteHelp we provide a variety of offerings to help you increase your social impact. We're the key to Greece's charitable sector. Work with us to develop a unique program that fits your brand and original objectives.</p>
            <ul class="nav soft--bottom">
              <li><a href="" class="font-base font-fredoka push-half--right">Donate for a project</a></li>
              <li><a href="" class="font-fredoka font-brand push-half--right">Gift Card</a></li>
              <li><a href="" class="font-base font-fredoka">Scheduled Giving</a></li>
            </ul>

            <h3 class="font-brand">PARTNER WITH US</h3>
            <p>ChasteHelp is a newcomer non-profit social enterprise. We have a wide array of solutions to help you promote charitable giving and enhance your organizations' overall charitable impact. Whether your organisation is a non-profit or for-profit, working together, we can increase charitable giving in Greece turning any point of contact into an opportunity for social good.</p>
            <p>Ready to Get Started?</p>
            <ul class="nav soft--bottom">
              <li><a href="" class="font-base font-fredoka push-half--right">Contact Us</a></li>
            </ul>

            <h3 class="font-brand">SUPPORT US</h3>
            <p>ChasteHelp plays a key role in the Greek charitable sector. We work hard to provide the trustworthy Greek charities with online tools for donating and fundraising.</p>
            <p>As a newcomer non-profit social enterprise, ChasteHelp is exclusively a self-funded charitable organisation, with our funding coming from the optional donations made by the donors that use our platform. With your additional support, we can do even more to better serve charities and donors, and ultimately increase charitable giving in Greece.</p>
            <ul class="nav soft--bottom">
              <li><a href="" class="font-base font-fredoka push-half--right">Donate to ChasteHelp</a></li>
            </ul>

        </div>
    </section>
</main>
<?php include 'global/footer.php' ?>
