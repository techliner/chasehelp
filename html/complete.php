<?php include 'global/head.php' ?>
<?php include 'global/header-no-logo.php' ?>
<main>
    <section class="background background__image--header">
        <div class="container">
            <h1 class="font-cap font-base epsilon flush--bottom soft-half">Fundraise</h1>
        </div>
    </section>
    <section class="background background__image background__image--grid soft--ends">
        <div class="container soft-half--top">
            <div class="grid">
                <div class="grid__item two-twelfths text--center">
                    <img src="img/icons/tick.svg" width="100px" class="push-half--right">
                </div><!--
             --><div class="grid__item ten-twelfths hard--left">
                    <h2 class="font-calibri font-cap delta push--top">THANK YOU! THE SUBMISSION OF YOUR PROPOSAL IS COMPLETED</h2>
                    <p>Next steps will follow and you will be contacted accordingly :</p>
                    <ol class="list-numeric">
                        <li class="grid__item one-third"><span class="list-numeric--item background background__grey-light--reverse">01</span><p class="soft--sides soft-half--top">A First Review and meeting with those who will be chosen to submit a completed proposal plan.</p></li><!--

                        --><li class="grid__item one-third"><span class="list-numeric--item background background__grey-light--reverse font-green">02</span><p class="soft--sides soft-half--top">Our final decision making and legal agreement with the partner after the proposals' review and due diligence.</p></li><!--

                        --><li class="grid__item one-third"><span class="list-numeric--item background background__grey-light--reverse font-base">03</span><p class="soft--sides soft-half--top">Upload the project on<br/>CHASTEHELP</p></li>
                    </ol>

                </div>

            </div>
        </div>

    </section>


    <section class="background background__image background__image--grid">
        <div class="background background__grey--transparent">
            <div class="container">
                <div class="grid">
                    <div class="grid__item one-half font-white soft-half--ends background__image background__image--kite-white"><h4 class="font-cap beta flush--bottom soft--ends triple-padding-left">all projects</h4></div><!--
                 --><div class="grid__item one-half font-white soft-half--ends background__image background__image--kite-white"><h4 class="font-cap beta flush--bottom soft--ends triple-padding-left">all fundraisers</h4></div>
                    <div class="grid__item one-half font-white soft-half--ends background__image background__image--kite-cloud"><h4 class="font-cap beta flush--bottom soft--ends triple-padding-left">completed projects</h4></div><!--
                 --><div class="grid__item one-half font-white soft-half--ends background__image background__image--bird"><h4 class="font-cap beta flush--bottom soft--ends triple-padding-left">my donations</h4></div>
                </div>
            </div>
        </div>
    </section>

</main>
<?php include 'global/footer.php' ?>
