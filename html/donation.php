<?php include 'global/head.php' ?>
<?php include 'global/header-no-logo.php' ?>
<main>
    <section class="background background__image--header">
        <div class="container">
            <h1 class="font-cap font-brand epsilon flush--bottom soft-half">< back to projects</h1>
        </div>
    </section>
    <section class="background background__image background__image--grid soft--ends">
        <div class="container">
            <div class="grid">
                <div class="grid__item one-twelfth">
                    <ul class="nav nav--stacked nav--social-icons text--center">
                        <li class="push-half--bottom">SHARE</li>
                        <li><a href=""><img src="img/share/plus.svg"></a></li>
                        <li><a href=""><img src="img/share/fbook.svg"></a></li>
                        <li><a href=""><img src="img/share/twitter.svg"></a></li>
                        <li><a href=""><img src="img/share/mail.svg"></a></li>
                    </ul>
                </div><!--
             --><div class="grid__item eleven-twelfths background background__white box-shadow">
                    <div class="push--sides soft--sides">
                        <h2 class="font-calibri font-cap text--center">title of dontaion</h2>
                        <h3 class="font-calibri font-cap epsilon flush--bottom">donation amount</h3>
                        <h4 class="font-calibri font-cap epsilon flush--bottom">select amount:</h4>

                        <div class="grid__item two-thirds hard--left">
                            <div class="grid__item one-fifth hard--left soft-half--right text--center">
                                <a href="" class="background background__grey push-half--bottom link-block alpha font-brand font-fredoka soft--ends">1£</a>
                                <a href="" class="background background__grey link-block alpha font-fredoka soft--ends font-brand">40£</a>
                            </div><!--
                         --><div class="grid__item one-fifth hard--left soft-half--right text--center">
                                <a href="" class="background background__grey push-half--bottom link-block alpha font-brand font-fredoka soft--ends">10£</a>
                                <a href="" class="background background__grey link-block alpha font-fredoka soft--ends font-brand">50£</a>
                            </div><!--
                         --><div class="grid__item one-fifth hard--left soft-half--right text--center">
                                <a href="" class="background background__grey push-half--bottom link-block alpha font-brand font-fredoka soft--ends">15£</a>
                                <a href="" class="background background__grey link-block alpha font-fredoka soft--ends font-brand">60£</a>
                            </div><!--
                            --><div class="grid__item one-fifth hard--left soft-half--right text--center">
                                   <a href="" class="background background__grey push-half--bottom link-block alpha font-brand font-fredoka soft--ends">20£</a>
                                   <a href="" class="background background__grey link-block alpha font-fredoka soft--ends font-brand">70£</a>
                               </div><!--
                         --><div class="grid__item one-fifth hard--left soft-half--right text--center">
                                <a href="" class="background background__grey push-half--bottom link-block alpha font-brand font-fredoka soft--ends">30£</a>
                                <a href="" class="background background__grey link-block alpha font-fredoka soft--ends font-brand">100£</a>
                            </div>

                        </div><!--
                     --><div class="grid__item one-third">
                            <div class="grid__item one-third hard--left soft-half--right text--center"></div><!--
                         --><div class="grid__item two-thirds hard--left text--center">
                                <div class="background background__grey soft--ends">
                                    <h3 class="font-calibri">Enter amount</h3>
                                    <input type="text" class="text-input background background__white four-fifths push--sides" placeholder="£" />
                                    <p class="milli">£1 minimum donation</p>

                                </div>

                            </div>
                        </div>


                    <h3 class="font-calibri font-cap epsilon push--top">personal details</h3>


                    <form class="soft--bottom">
                        <ul class="form-fields multi-list  two-cols">
                            <li>
                                <label>Email</label>
                                <input class="text-input background background__grey two-thirds">
                            </li>
                            <li>
                                <label>Donation carrier <small>(for companies & foundations)</small></label>
                                <input class="text-input background background__grey two-thirds">
                            </li>
                            <li>
                                <div class="grid__item one-fifth hard--left">
                                    <label>Title</label>
                                    <select id="country" class="background background__grey one-whole">
                                        <option>Mr</option>
                                        <option>Mrs</option>
                                        <option>Other</option>
                                    </select>
                                </div><!--
                             --><div class="grid__item two-thirds">
                                    <label>First name</label>
                                    <input class="text-input background background__grey two-thirds">
                                </div>
                            </li>
                            <li>
                                <label for="asd">Subscribe to Newsletter</label>
                                <input type="checkbox" id="by-email" class="push-half--ends">
                            </li>
                            <li class="clearfix">
                                <label>Last name</label>
                                <input class="text-input background background__grey two-thirds">
                            </li>
                        </ul>
                    </form>

                    <p>Make your donation in memory, honour or in celebration of someone. You can choose from a variety of e-cards for every occasion, personalize them to fit the recipient, and schedule the delivery day.</p>

                    <ul class="form-fields soft--bottom">
                        <li>

                            <ul class="check-list  multi-list  two-cols">
                                <li>
                                    <input type="radio" name="frequency" id="weekly"> <label for="weekly">No, thanks</label>
                                </li>
                                <li>
                                    <input type="radio" name="frequency" id="daily"> <label for="daily">Yes</label>
                                </li>
                            </ul>
                        </li>
                    </ul>

                    <div class="grid push--top">
                        <div class="grid__item two-thirds">
                            <div class="background background__base--light soft">
                                <h3 class="font-cap font-calibri flush--bottom soft-half">do you wish to donate to chastehelp ?</h3>
                            </div>
                        </div><!--
                     --><div class="grid__item one-third hard--left">
                            <div class="background background__base soft text--center">
                                <select id="country" class="background background__white two-thirds push-half--top">
                                    <option>SELECT AMOUNT</option>
                                    <option>1£</option>
                                    <option>2£</option>
                                    <option>4£</option>
                                    <option>6£</option>
                                    <option>8£</option>
                                    <option>10£</option>
                                    <option>15£</option>
                                    <option>20£</option>
                                </select>

                            </div>
                        </div>
                    </div>

                    <a href="" class="background background__brand text--center soft push--top link-block">
                        <h3 class="font-calibri font-cap font-white flush--bottom">Donate</h3>
                        <p class="milli font-white flush--bottom">(transfer to paypal)</p>
                    </a>


                </div>

            </div>
        </div>
    </section>

</main>
<?php include 'global/footer.php' ?>
