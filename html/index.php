<?php include 'global/head.php' ?>
<?php include 'global/header.php' ?>
<main>
    <section class="background background__grey-light soft--bottom">
        <div class="container">
            <div class="hero__section background background__image background__image--main">
                <div class="hero__header">
                    <h1 class="font-base">Greece's Platform <br />for donating and <br /> Fundraising online</h1>
                    <p class="text--right font-brand">You don't just dontate <br />You give the choice<br /> to those who really need it</p>
                </div>

                <h4 class="uppercase hero__title font-brand">How it works</h4>

                <div class="hero__facts hero__facts--one text--center">
                    <h4 class="flush--bottom font-brand">01</h4>
                    <p class="milli">Lorem Ipsum is simply</p>
                </div>
                <div class="hero__facts hero__facts--two text--center">
                    <h4 class="flush--bottom font-base">02</h4>
                    <p class="milli">Lorem Ipsum is simply</p>
                </div>
                <div class="hero__facts hero__facts--three text--center">
                    <h4 class="flush--bottom font-green">03</h4>
                    <p class="milli">Lorem Ipsum is simply</p>
                </div>
                <div class="hero__facts hero__facts--four text--center">
                    <h4 class="flush--bottom font-red">04</h4>
                    <p class="milli">Lorem Ipsum is simply</p>
                </div>
            </div>
            <div id="search-form"></div>
        </div>
    </section>

    <section class="background background__grey-light--reverse background__image--project">
        <div class="container">
            <div class="grid">
                <div class="grid__item one-quarter text--right"><h3 class="uppercase soft--top font-brand">Project AAA</h3></div><!--
             --><div class="grid__item two-quarters soft--top"><p class="soft--sides">Lorem Ipsum is simply dummy text of the printing. Ipsum has been the industry's standard dummy text ever since the 1500s, when an unkno</p>
                </div><!--
             --><div class="grid__item one-quarter soft--top"><div class="soft--top"> <a href="" class="font-brand">see more >></a></div></div>
            </div>
        </div>
    </section>

    <section class="background background__base--gradient">
        <div class="container">
            <div class="grid">
                <div class="grid__item one-half">
                    <div class="promo__section background background__image background__image--gift">
                        <div class="grid soft--ends">
                            <div class="grid__item one-half"></div><!--
                        --><div class="grid__item one-half soft-half--top">
                                <h3 class="uppercase epsilon soft--top  push--top font-red">Explore charitable gifts</h3>
                                <p class="soft--sides push-half--bottom">Lorem Ipsum is simply dummy text of the printing. Ipsum has been the industry's standard dummy text ever since the 1500s, when an unkno</p>
                                <a href="" class="soft--sides font-brand">Explore projects</a>
                            </div>
                        </div>
                    </div>
                </div><!--
             --><div class="grid__item one-half">
                    <div class="promo__section background background__image background__image--schedule">
                        <div class="grid soft--ends">
                            <div class="grid__item one-half"></div><!--
                        --><div class="grid__item one-half soft-half--top">
                                <h3 class="uppercase epsilon soft--top push--top font-red">Explore scheduled giving</h3>
                                <p class="soft--sides push-half--bottom">Lorem Ipsum is simply dummy text of the printing. Ipsum has been the industry's standard dummy text ever since the 1500s, when an unkno</p>
                                <a href="" class="soft--sides font-brand">Explore projects</a>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="background background__image background__image--grid">
        <div class="background background__grey--transparent">
            <div class="container soft--ends ">
                <div class="grid soft--ends">
                    <div class="grid__item one-quarter">
                        <div class="background background__red soft font-white">
                            <h3 class="delta">Create a Fundraiser</h3>
                            <p class="font-white">Lorem Ipsum is simply dummy text of the printing. Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries</p>
                        </div>
                    </div><!--
                 --><div class="grid__item one-quarter font-white">
                        <div class="background background__green soft">
                            <h3 class="delta">Find a Fundraiser</h3>
                            <p class="font-white">Lorem Ipsum is simply dummy text of the printing. Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries</p>
                        </div>
                    </div><!--
                 --><div class="grid__item one-quarter font-white">
                        <div class="background background__blue--transparent soft">
                            <h3 class="delta">All the Fundraiser</h3>
                            <p class="font-white">Lorem Ipsum is simply dummy text of the printing. Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries</p>
                        </div>
                    </div><!--
                 --><div class="grid__item one-quarter">
                        <div class="share-story soft--left">
                            <h3>Share your giving story</h3>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                            <img src="img/backgrounds/share_frpage.png">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
<?php include 'global/footer.php' ?>
