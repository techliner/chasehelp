<?php include 'global/head.php' ?>
<?php include 'global/header-no-logo.php' ?>
<main>
    <section class="background background background__image--header">
        <div class="container">
            <h1 class="font-cap font-brand epsilon flush--bottom soft-half">< back to projects</h1>
        </div>
    </section>
    <section class="background background__image background__image--grid soft--ends">
        <div class="container">
            <div class="grid">
                <div class="grid__item one-twelfth"></div><!--
             --><div class="grid__item eleven-twelfths background background__white box-shadow soft">
                    <div class="mydonations">
                        <h2 class="font-calibri font-cap text--center">my donations</h2>

                        <div class="grid__item one-half hard--left">
                            <ul class="nav nav--stacked">
                                <li class="gamma font-fredoka">email@ttest.com</li>
                                <li class="beta font-fredoka">Mrs test test</li>
                            </ul>

                            <a href="" class="btn btn--small btn--hard btn--negative soft--sides">Edit</a>
                        </div><!--
                     --><div class="grid__item one-half">
                            <span class="font-cap gamma">total amount donated: </span><span class="font-fredoka beta">£50</span>

                        </div>

                        <div class="push--top soft--top">
                            <h3 class="font-calibri font-grey font-cap delta">Recent Activties</h3>
                            <div class="grid__item one-half hard--left">
                              <div class="soft--right push-half--right">
                                <div class="background background__grey--light background__image--share--green soft">
                                    <h3 class="font-calibri flush--bottom">header</h3>
                                    <p class="flush--bottom beta font-fredoka">£20</p>
                                </div>
                                <div class="soft-half background background__green">
                                    <ul class="nav nav--social-icons nav--social-icons-valign  flush--bottom">
                                        <li class="font-cap push--right">share</li>
                                        <li class="push-half--right"><a href=""><img src="img/share/mail_white.svg"></a> </li>
                                        <li class="push-half--right"><a href=""><img src="img/share/fbook_white.svg"></a></li>
                                        <li><a href=""><img src="img/share/twitter_white.svg"></a></li>
                                        <li class="font-cap font-white push--left">new donation</li>
                                    </ul>
                                </div>
                              </div>
                            </div><!--
                         --><div class="grid__item one-half">
                              <div class="soft-left push-half--left">
                                <div class="background background__image--share--yellow background__grey--light soft">
                                    <h3 class="font-calibri flush--bottom">header</h3>
                                    <p class="flush--bottom beta font-fredoka">£20</p>
                                </div>
                                <div class="soft-half background  background__base">
                                    <ul class="nav nav--social-icons nav--social-icons-valign flush--bottom">
                                        <li class="font-cap push--right">share</li>
                                        <li class="push-half--right"><a href=""><img src="img/share/mail_white.svg"></a> </li>
                                        <li class="push-half--right"><a href=""><img src="img/share/fbook_white.svg"></a></li>
                                        <li><a href=""><img src="img/share/twitter_white.svg"></a></li>
                                        <li class="font-cap font-white push--left">new donation</li>
                                    </ul>
                                </div>
                              </div>
                            </div>
                        </div>


                        <div class="grid push--top">
                            <div class="grid__item two-thirds">
                                <div class="background background__grey--light background__image--share--kite soft--left soft-half--ends">
                                    <p class="delta font-cap font-calibri flush--bottom text--right font-grey-dark grid__item two-thirds">donations to chastehelp</p><!--
                                 --><p class="text--right gamma grid__item one-third flush--bottom soft--right">£1000</p>
                                </div>
                            </div><!--
                        --><a class="grid__item one-third hard--left background background__base text--center soft-half delta font-white font-cap" href="">
                                new donation
                            </a>
                        </div>
                    </div>







                </div>

            </div>
        </div>
    </section>

</main>
<?php include 'global/footer.php' ?>
