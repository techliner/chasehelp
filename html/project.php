<?php include 'global/head.php' ?>
<?php include 'global/header-no-logo.php' ?>
<main>
    <section class="background background__image--header">
        <div class="container">
            <h1 class="font-cap font-brand epsilon flush--bottom soft-half">Projects >> children with abilities >> Kivotos</h1>
        </div>
    </section>
    <section class="background background__image background__image--grid soft--ends">
        <div class="container soft-half--top">
            <div class="grid">
                <div class="grid__item one-twelfth">
                    <ul class="nav nav--stacked nav--social-icons text--center">
                        <li class="push-half--bottom">SHARE</li>
                        <li><a href=""><img src="img/share/plus.svg"></a></li>
                        <li><a href=""><img src="img/share/fbook.svg"></a></li>
                        <li><a href=""><img src="img/share/twitter.svg"></a></li>
                        <li><a href=""><img src="img/share/mail.svg"></a></li>
                    </ul>
                </div><!--
             --><div class="grid__item eight-twelfths hard--left">
                    <div class="background background__white box-shadow border-green soft--right soft--left">
                        <h2 class="font-calibri font-cap">title of dontaion</h2>
                        <ul class="nav push-half--bottom">
                            <li class="grid__item one-third hard--left font-cap"><span class="zeta">children with special abilities</li><!--
                         --><li class="grid__item one-fifth hard--left"><span class="gamma"><strong>10%</strong></span> funded</li><!--
                         --><li class="grid__item one-fifth hard--left"><span class="gamma font-grey"><strong>£5000</strong></li><!--
                         --><li class="grid__item one-fifth hard--left"><span class="gamma"><strong>£1100</strong></span> to go</li>
                        </ul>
                        <div class="grid">
                            <div class="grid__item one-third"><img src="http://placehold.it/550x550"> </div><!--
                         --><div class="grid__item two-thirds">
                                <p class="float--left gamma">Ends 19/05/2016</p>
                                <a class="btn btn--small btn--hard btn--negative float--right font-cap soft--sides">donate</a>
                                <p class="float--none clearfix">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the rele</p>
                            </div>
                        </div>
                    </div>
                    <div class="soft--top">
                        <img src="img/icons/tick.svg" width="30px" class="push-half--right"><span class="alpha font-green-dark">asdsdasdas</span>
                    </div>
                </div><!--
             --><div class="grid__item three-twelfths">
                    <div class="background background__white soft-half">
                        <h3 class="font-calibri font-cap zeta push-half--bottom">Would you like to support Chastlehelp.org ?</h3>
                    </div>
                    <div class="background background__base soft-half">
                        <select id="country" class="background background__white three-fifths zeta grid__item">
                            <option>SELECT AMOUNT</option>
                            <option>1</option>
                            <option>2</option>
                        </select><!--
                     --><p class="grid__item two-fifths font-cap font-white">donate</p>
                    </div>

                    <div class="soft-half">
                        <span class="grid__item one-half delta hard--left font-cap epsilon soft--top">share your giving story </span><!--
                     --><img src="img/backgrounds/share_frpage.png" width="70px" class="grid__item one-half">
                    </div>




                </div>


            </div>
        </div>

    </section>


    <section class="background background__image background__image--grid">
        <div class="background background__grey--transparent">
            <div class="container">
                <div class="grid">
                    <div class="grid__item one-half font-brand soft--ends background__image background__image--right background__image--gift--small text--center"><h4 class="font-cap ultra flush--bottom soft--ends">Explore charitable gifts</h4></div><!--
                 --><div class="grid__item one-half font-brand soft--ends background__image background__image--right background__image--schedule--small text--center"><h4 class="font-cap ultra flush--bottom soft--ends">Explore scheduled giving</h4></div>
                </div>
            </div>
        </div>
    </section>

</main>
<?php include 'global/footer.php' ?>
