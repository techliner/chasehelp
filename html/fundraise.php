<?php include 'global/head.php' ?>
<?php include 'global/header-no-logo.php' ?>
<main>
    <section class="background background__image--projects soft--bottom">
        <div class="container text--center soft--top background background__image background__image--top background__image--explore">
            <h1 class="font-cap soft--top font-base">fundraise</h1>
        </div>
    </section>

    <section class="background background__image background__image--grid soft--ends">
        <div class="container">
            <div class="grid">
                <h2 class="font-calibri font-cap delta push--top">create a fundraiser</h2>
                <p>ChasteHelp wants to give Foundations, Non-for-Profit Organisations as well as professionals with more limited access το funding, the opportunity to both choose and form initiatives uploaded on our platform in order to get financed. The selection procedure of an initiative includes the following steps:</p>

                <ol class="list-numeric">
                    <li class="grid__item one-quarter"><span class="list-numeric--item background background__grey-light--reverse">01</span><p class="soft--sides soft-half--top">Proposal & Submission</p></li><!--
                 --><li class="grid__item one-quarter"><span class="list-numeric--item background background__grey-light--reverse font-green">02</span><p class="soft--sides soft-half--top">A First Review and meeting with those who will be chosen to submit a completed proposal plan.</p></li><!--
                 --><li class="grid__item one-quarter"><span class="list-numeric--item background background__grey-light--reverse font-base">03</span><p class="soft--sides soft-half--top">Our final decision making and legal agreement with the partner after the proposals' review and due diligence.</p></li><!--
                 --><li class="grid__item one-quarter"><span class="list-numeric--item background background__grey-light--reverse font-red">04</span><p class="soft--sides soft-half--top">Upload the project on CHASTEHELP</p></li>
                </ol>


                <h3 class="font-calibri font-cap delta push--top">Submit your proposal</h3>
                <div class="grid__item one-whole background background__white box-shadow">


                    <div class="mydonations hard--bottom">

                        <div class="push--sides soft--sides">

                            <h3 class="font-calibri font-cap epsilon push--top push--left">are you ?</h3>


                            <form>
                                <ul class="check-list  multi-list push--left push--bottom">
                                    <li class="push--right">
                                        <input type="radio" name="frequency" id="foundation"> <label for="foundation">A Foundation</label>
                                    </li>
                                    <li class="push--sides">
                                        <input type="radio" name="nonprofit" id="nonprofit"> <label for="daily">A Non - Profit Organization</label>
                                    </li>
                                    <li class="push--sides">
                                        <input type="radio" name="frequency" id="citizens"> <label for="citizens">A group of citizens</label>
                                    </li>
                                    <li class="push--left">
                                        <input type="radio" name="frequency" id="professional"> <label for="professional">A professional</label>
                                    </li>
                                </ul>
                                <ul class="form-fields multi-list  two-cols push--bottom push--left">
                                    <li>
                                        <label>Email</label>
                                        <input class="text-input background background__grey two-thirds">
                                    </li>
                                    <li>
                                        <label>Phone no.</label>
                                        <input class="text-input background background__grey two-thirds">
                                    </li>
                                    <li>
                                        <label>Name</label>
                                        <input class="text-input background background__grey two-thirds">
                                    </li>
                                    <li>
                                        <label for="asd">Terms of use</label>
                                        <input type="checkbox" id="by-email" class="push-half--ends">
                                    </li>
                                    <li class="clearfix">
                                        <label>Surname</label>
                                        <input class="text-input background background__grey two-thirds">
                                    </li>
                                    <div class="grid">
                                        <div class="grid__item four-fifths">
                                            <label class="clearfix">Brief description of proposal</label>
                                            <textarea rows="4" cols="75" class="float--left background background__grey" ></textarea>
                                        </div><!--
                                     --><div class="grid__item one-fifth">
                                            <label>Upload Doc</label>
                                            <input type="file" name="pic" accept="image/*" class="background__image--doc upload">
                                        </div>
                                    </div>


                                </ul>

                                <button class="btn btn--hard background background__base text--center soft-half push--top font-white font-cap one-whole ">Submit</button>
                            </form>



                        </div>
                    </div>
                </div>
            </div>
    </section>

</main>
<?php include 'global/footer.php' ?>
