<header class="background background__brand">
    <div class="container background background__brand">
        <div class="grid soft-half--ends">
            <div class="grid__item one-quarter">
                <img src="img/logo.png" alt="logo">
            </div><!--
         --><div class="grid__item three-quarters">
                <div class="grid__item  one-whole">
                    <ul class="nav nav--social-icons float--right push-half--bottom">
                        <li><a href="" title="googleplus"><img src="img/icons/google_plus.svg" alt="googleplus"></a></li>
                        <li><a href="" title="facebook" class="push-half--sides"><img src="img/icons/facebook.svg" alt="facebook"></a></li>
                        <li><a href="" title="twitter"><img src="img/icons/twitter.svg" alt="twitter"></a></li>
                    </ul>
                    <ul class="nav nav--header nav--capitalise nav--with-icon float--right push-half--bottom">
                        <li><a href="" title="gr" class="seperator">gr</a></li>
                        <li><a href="" title="en">en</a></li>
                        <li><a href="" title="my donation" class="push-half--sides active">my donations <img src="img/icons/magicw.svg" alt="my donation"> </a></li>
                        <li><a href="" title="sign up" class="push--right">sign up</a></li>
                    </ul>
                </div>
                <div class="grid__item one-whole">
                    <nav>
                        <ul class="nav nav--header nav--capitalise float--right flush--bottom">
                            <li><a href="" title="about chastlehelp" class="push--sides">about chastlehelp</a></li>
                            <li><a href="" title="why chastlehelp" class="push--sides">why chastlehelp</a></li>
                            <li><a href="" title="donate" class="push--sides">donate</a></li>
                            <li><a href="" title="fundraise" class="push--left">fundraise</a></li>
                        </ul>
                    </nav>
                </div>
            </div>

        </div>
    </div>
</header>
