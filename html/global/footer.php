<footer>

    <section class="background background__image background__image--grid">
        <div class="container soft--ends background background__image--footer">
            <div class="grid soft--ends">
                <div class="grid__item one-fifth">
                    <h3 class="delta font-brand uppercase epsilon push-half--bottom">About chastleHelp</h3>
                    <ul class="nav nav--stacked">
                        <li><a href="" title="What we do" class="font-brand">What we do</a></li>
                        <li><a href="" title="How we act" class="font-brand">How we act</a></li>
                        <li><a href="" title="What makes us different" class="font-brand">What makes us different</a></li>
                        <li><a href="" title="Our team" class="font-brand">Our team</a></li>
                        <li><a href="" title="Jobs" class="font-brand">Jobs</a></li>
                        <li><a href="" title="Contact Us" class="font-brand">Contact Us</a></li>
                    </ul>
                </div><!--
             --><div class="grid__item one-fifth">
                    <h3 class="delta font-brand uppercase epsilon push-half--bottom">why chastlehelp</h3>
                    <ul class="nav nav--stacked">
                        <li><a href="" title="Ways to give" class="font-brand">Ways to give</a></li>
                        <li><a href="" title="No Fees" class="font-brand">No Fees</a></li>
                        <li><a href="" title="For Donors" class="font-brand">For Donors</a></li>
                        <li><a href="" title="For Charities" class="font-brand">For Charities</a></li>
                        <li><a href="" title="For Corporation" class="font-brand">For Corporations</a></li>
                        <li><a href="" title="Partner with Us" class="font-brand">Partner with Us</a></li>
                        <li><a href="" title="Support" class="font-brand">Support Us</a></li>
                    </ul>
                </div><!--
             --><div class="grid__item one-fifth">
                    <h3 class="delta font-brand uppercase epsilon push-half--bottom">Donate</h3>
                    <ul class="nav nav--stacked">
                        <li><a href="" title="Donate for a Project" class="font-brand">Donate for a Project</a></li>
                        <li><a href="" title="Donate to a Charity" class="font-brand">Donate to a Charity</a></li>
                        <li><a href="" title="Gift a Charity Gift Card" class="font-brand">Gift a Charity Gift Card</a></li>
                        <li><a href="" title="Give Once or Monthly" class="font-brand">Give Once or Monthly</a></li>
                    </ul>
                </div><!--
             --><div class="grid__item one-fifth">
                    <h3 class="delta font-brand uppercase epsilon push-half--bottom">Fundraise</h3>
                    <ul class="nav nav--stacked">
                        <li><a href="" title="Create a Fundraiser" class="font-brand">Create a Fundraiser</a></li>
                        <li><a href="" title="Find a Fundraiser" class="font-brand">Find a Fundraiser</a></li>
                        <li><a href="" title="All the Fundraisers" class="font-brand">All the Fundraisers</a></li>
                    </ul>
                </div><!--
            --><div class="grid__item one-fifth">
                    <h3 class="delta font-brand uppercase epsilon push-half--bottom">Follow us</h3>
                    <ul class="nav nav--stacked">
                        <li><a href="mailto:info@chastehelp.org" class="font-brand">info@chastehelp.org</a></li>
                        <li><a href="tel:02072633367" class="font-brand">020 7263 3367</a></li>
                        <li><a href="tel:02072818213" class="font-brand">020 7281 8213</a></li>
                        <li><a href="" title="Facebook" class="font-brand">Facebook</a></li>
                        <li><a href="" title="Twitter" class="font-brand">Twitter</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <div class="background background__blue">
        <div class="container">
          <p class="text--right flush--bottom font-white soft-half--ends">chastlehelp.org</p>
        </div>
    </div>

</footer>


<script src="js/bundle.js"></script>
<!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
<script>
    (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
        function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
        e=o.createElement(i);r=o.getElementsByTagName(i)[0];
        e.src='https://www.google-analytics.com/analytics.js';
        r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
    ga('create','UA-XXXXX-X','auto');ga('send','pageview');
</script>
</body>
</html>
