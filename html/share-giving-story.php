<?php include 'global/head.php' ?>
<?php include 'global/header.php' ?>
<main>
    <section class="background background__image--header">
        <div class="container">
            <h1 class="font-cap font-base epsilon flush--bottom soft-half">Share your giving story</h1>
        </div>
    </section>
    <section class="background background__image--grid soft--bottom">
        <div class="container soft--top">

            <h2 class="font-calibri font-cap delta push--top text--center">Why not share your experience with your friends:</h2>
            <div class="grid">
                <div class="grid__item one-half"><p class="font-cap font-fredoka font-brand delta text--right flush--bottom background__image--share--yellow background__image--share--yellow--left">Share your donation activity</p>
                    <ul class="nav nav--social-icons float--right">
                        <li class="font-cap font-fredoka font-brand delta push-half--right">on</li>
                        <li><a href="" title="googleplus"><img src="img/share/plus.svg" alt="googleplus"></a></li>
                        <li><a href="" title="facebook" class="push-half--sides"><img src="img/share/fbook.svg" alt="facebook"></a></li>
                        <li><a href="" title="twitter"><img src="img/share/mail.svg" alt="mail"></a></li>
                    </ul>
                </div><!--
             --><div class="grid__item one-half"><p class="font-cap font-fredoka font-brand delta">or Manage what you share <br /> through <a href="" class="font-base">"my donations"</a></p></div>
            </div>
            <h3 class="font-calibri font-cap delta push--top text--center">or feel like giving us some feedback? answer the questionnaire below ?</h3>

            <div class="grid__item one-whole background background__white box-shadow">


                <div class="mydonations hard--bottom">

                    <div class="push--sides soft--sides">

                        <h3 class="font-calibri font-cap epsilon push--top">What did you like about your experience with chastehelp ?</h3>


                        <form>
                            <ul class="check-list  push--left push--bottom">
                                <li class="push--ends">
                                    <input type="radio" name="frequency" id="foundation"> <label for="foundation">A Foundation</label>
                                </li>
                                <li class="push--ends">
                                    <input type="radio" name="nonprofit" id="nonprofit"> <label for="daily">A Non - Profit Organization</label>
                                </li>
                                <li class="push--ends">
                                    <input type="radio" name="frequency" id="citizens"> <label for="citizens">A group of citizens</label>
                                </li>
                                <li class="push--ends">
                                    <input type="radio" name="frequency" id="professional"> <label for="professional">A professional</label>
                                </li>
                            </ul>

                            <h3 class="font-calibri font-cap epsilon push--top">What did you dislike about your experience with chastehelp ?</h3>


                            <ul class="check-list  push--left push--bottom">
                                <li class="push--ends">
                                    <input type="radio" name="frequency" id="foundation"> <label for="foundation">A Foundation</label>
                                </li>
                                <li class="push--ends">
                                    <input type="radio" name="nonprofit" id="nonprofit"> <label for="daily">A Non - Profit Organization</label>
                                </li>
                                <li class="push--ends">
                                    <input type="radio" name="frequency" id="citizens"> <label for="citizens">A group of citizens</label>
                                </li>
                                <li class="push--ends">
                                    <input type="radio" name="frequency" id="professional"> <label for="professional">A professional</label>
                                </li>
                            </ul>

                            <h3 class="font-calibri font-cap epsilon push--top">What would you change in chastehelp ?</h3>


                            <ul class="check-list  push--left push--bottom cf">
                                <li class="push--ends">
                                    <input type="radio" name="frequency" id="foundation"> <label for="foundation">A Foundation</label>
                                </li>
                                <li class="push--ends">
                                    <input type="radio" name="nonprofit" id="nonprofit"> <label for="daily">A Non - Profit Organization</label>
                                </li>
                                <li class="push--ends">
                                    <input type="radio" name="frequency" id="citizens"> <label for="citizens">A group of citizens</label>
                                </li>
                                <li class="push--ends">
                                    <textarea rows="4" cols="75" class="background background__grey push--left" ></textarea>
                                </li>
                            </ul>

                            <h3 class="font-calibri font-cap epsilon push--top">Would you donate through chastehelp again?</h3>


                            <ul class="check-list  push--left push--bottom">
                                <li class="push--ends">
                                    <input type="radio" name="frequency" id="foundation"> <label for="foundation">A Foundation</label>
                                </li>
                                <li class="push--ends">
                                    <input type="radio" name="nonprofit" id="nonprofit"> <label for="daily">A Non - Profit Organization</label>
                                </li>
                            </ul>




                            <button class="btn btn--hard background background__base text--center soft-half push--top font-white font-cap one-whole ">Submit</button>
                        </form>



                    </div>
                </div>
            </div>

        </div>
    </section>
    <section class="background background__image background__image--grid">
        <div class="background background__grey--transparent">
            <div class="container">
                <div class="grid">
                    <div class="grid__item one-half font-white soft-half--ends background__image background__image--kite-white"><h4 class="font-cap beta flush--bottom soft--ends triple-padding-left">all projects</h4></div><!--
                 --><div class="grid__item one-half font-white soft-half--ends background__image background__image--kite-white"><h4 class="font-cap beta flush--bottom soft--ends triple-padding-left">all fundraisers</h4></div>
                </div>
            </div>
        </div>
    </section>
</main>
<?php include 'global/footer.php' ?>
