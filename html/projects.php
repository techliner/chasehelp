<?php include 'global/head.php' ?>
<?php include 'global/header-no-logo.php' ?>
<main>
    <section class="background background__image--projects soft--bottom">
        <div class="container text--center soft--top background background__image background__image--top background__image--explore">
            <h1 class="font-cap soft--top font-green">All projects</h1>
        </div>
    </section>
    <section class="background background__image background__image--grid soft--bottom">
        <div class="container soft--sides">
            <form class="soft--top soft-half--bottom">
                <ul class="form-fields">
                    <li>
                        <select id="country" class="background background__grey one-third soft-half--ends push--right">
                            <option>SELECT CATEGORY</option>
                            <option>US</option>
                            <option>Other</option>
                        </select>
                        <input type="text" class="text-input background background__grey one-third push--right" placeholder="SEARCH PROJECTS">
                        <button class="btn btn--small btn--hard background background__base--transparent font-cap push--right">Search</button>
                    </li>
                </ul>
            </form>
            <div class="grid">
                <div class="grid__item one-third font-brand push-half--ends">
                    <div class="soft-half font-white background background__green--transparent">
                        <h3 class="font-calibri push-half--bottom">asdasd</h3>
                        <p class="font-black soft-half push-half--bottom background background__white">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem</p>
                        <h4 class="font-calibri push-half--bottom">aaa asdasd</h4>
                        <ul class="nav push-half--bottom">
                            <li class="grid__item one-half hard--left"><span class="gamma">10%</span> funded</li><!--
                         --><li class="grid__item one-half"><span class="gamma">£1100</span> to go</li>
                        </ul>
                        <div class="grid__item one-whole text--right">
                            <a class="btn btn--small btn--hard background background__white font-cap font-grey-dark" href="">Donate</a>
                        </div>
                    </div>

                </div><!--
             --><div class="grid__item one-third font-brand push-half--ends">
                    <div class="soft-half font-white background background__base--transparent">
                        <h3 class="font-calibri push-half--bottom">asdasd</h3>
                        <p class="font-black soft-half push-half--bottom background background__white">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem</p>
                        <h4 class="font-calibri push-half--bottom">aaa asdasd</h4>
                        <ul class="nav push-half--bottom">
                            <li class="grid__item one-half hard--left"><span class="gamma">10%</span> funded</li><!--
                         --><li class="grid__item one-half"><span class="gamma">£1100</span> to go</li>
                        </ul>
                        <div class="grid__item one-whole text--right">
                            <a class="btn btn--small btn--hard background background__white font-cap font-grey-dark" href="">Donate</a>
                        </div>
                    </div>
                </div><!--
             --><div class="grid__item one-third font-brand push-half--ends">
                    <div class="soft-half font-white background background__green--transparent">
                        <h3 class="font-calibri push-half--bottom">asdasd</h3>
                        <p class="font-black soft-half push-half--bottom background background__white">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem</p>
                        <h4 class="font-calibri push-half--bottom">aaa asdasd</h4>
                        <ul class="nav push-half--bottom">
                            <li class="grid__item one-half hard--left"><span class="gamma">10%</span> funded</li><!--
                         --><li class="grid__item one-half"><span class="gamma">£1100</span> to go</li>
                        </ul>
                        <div class="grid__item one-whole text--right">
                            <a class="btn btn--small btn--hard background background__white font-cap font-grey-dark" href="">Donate</a>
                        </div>
                    </div>
                </div>

                <div class="grid__item one-third font-brand push-half--ends">
                    <div class="soft-half font-white background background__base--transparent">
                        <h3 class="font-calibri push-half--bottom">asdasd</h3>
                        <p class="font-black soft-half push-half--bottom background background__white">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem</p>
                        <h4 class="font-calibri push-half--bottom">aaa asdasd</h4>
                        <ul class="nav push-half--bottom">
                            <li class="grid__item one-half hard--left"><span class="gamma">10%</span> funded</li><!--
                         --><li class="grid__item one-half"><span class="gamma">£1100</span> to go</li>
                        </ul>
                        <div class="grid__item one-whole text--right">
                            <a class="btn btn--small btn--hard background background__white font-cap font-grey-dark" href="">Donate</a>
                        </div>
                    </div>

                </div><!--
             --><div class="grid__item one-third font-brand push-half--ends">
                    <div class="soft-half font-white background background__green--transparent">
                        <h3 class="font-calibri push-half--bottom">asdasd</h3>
                        <p class="font-black soft-half push-half--bottom background background__white">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem</p>
                        <h4 class="font-calibri push-half--bottom">aaa asdasd</h4>
                        <ul class="nav push-half--bottom">
                            <li class="grid__item one-half hard--left"><span class="gamma">10%</span> funded</li><!--
                         --><li class="grid__item one-half"><span class="gamma">£1100</span> to go</li>
                        </ul>
                        <div class="grid__item one-whole text--right">
                            <a class="btn btn--small btn--hard background background__white font-cap font-grey-dark" href="">Donate</a>
                        </div>
                    </div>
                </div><!--
             --><div class="grid__item one-third font-brand push-half--ends">
                    <div class="soft-half font-white background background__base--transparent">
                        <h3 class="font-calibri push-half--bottom">asdasd</h3>
                        <p class="font-black soft-half push-half--bottom background background__white">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem</p>
                        <h4 class="font-calibri push-half--bottom">aaa asdasd</h4>
                        <ul class="nav push-half--bottom">
                            <li class="grid__item one-half hard--left"><span class="gamma">10%</span> funded</li><!--
                         --><li class="grid__item one-half"><span class="gamma">£1100</span> to go</li>
                        </ul>
                        <div class="grid__item one-whole text--right">
                            <a class="btn btn--small btn--hard background background__white font-cap font-grey-dark" href="">Donate</a>
                        </div>
                    </div>
                </div>

            </div>
        </div>


    </section>
    <section class="background background__image background__image--grid">
        <div class="background background__grey--transparent">
            <div class="container">
                <div class="grid">
                    <div class="grid__item one-third font-brand soft--ends background__image background__image--right background__image--gift--small"><h4 class="font-cap soft--ends push-half--ends ultra">Explore charitable gifts</h4></div><!--
                 --><div class="grid__item one-third font-brand soft--ends background__image background__image--right background__image--schedule--small"><h4 class="font-cap soft--ends push-half--ends ultra">Explore scheduled giving</h4></div><!--
                 --><div class="grid__item one-third font-brand soft--ends background__image background__image--right background__image--share"><h4 class="font-cap soft--ends push-half--ends ultra">share your giving story</h4></div>
                </div>
            </div>
        </div>
    </section>
</main>
<?php include 'global/footer.php' ?>
