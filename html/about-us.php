<?php include 'global/head.php' ?>
<?php include 'global/header.php' ?>
<main>
    <section class="background background__grey-light soft--bottom">
        <div class="container soft--top">
            <h1 class="font-base soft--top">About Chastehelp</h1>

            <h3 class="font-brand">WHAT WE DO</h3>
            <p>ChasteHelp makes giving simple, safe and trustworthy. We are a one-stop shop for charitable giving and fundraising. We act as an e-meeting point for non-profit organizations, corporations, freelancers and citizens who want to support or be supported by the initiations we develop on our platform. In this section, please explore all the ways you can give through ChasteHelp!</p>
            <ul class="nav soft--bottom">
              <li>See the <a href="" class="font-fredoka font-brand">due diligence in creating a Fundraiser</a></li>
            </ul>

            <p>ChasteHelp makes giving simple, safe and trustworthy. We are a one-stop shop for charitable giving and fundraising. We act as an e-meeting point for non-profit organizations, corporations, freelancers and citizens who want to support or be supported by the initiations we develop on our platform. In this section, please explore all the ways you can give through ChasteHelp!</p>
            <ul class="nav soft--bottom">
                <li>Please <a href="" class="font-fredoka font-base">donate for an initiative</a></li>
            </ul>

            <h3 class="font-brand">WHAT MAKES US UNIQUE</h3>
            <ul class="soft--bottom push-half--left">
              <li>Donate for A Project  Donate to A Charity or How to support us</li>
              <li>Donate for A Project  Donate to A Charity or How to support us</li>
            </ul>

            <h3 class="font-brand">OUR TEAM</h3>
            <div class="grid">
                <div class="grid__item one-fifth text--center">
                    <img src="img/logo.png" alt="logo">
                    <p><em>Founder & Ceo</em></p>
                </div><!--
         --><div class="grid__item four-fifths">
                    <p>ChasteHelp makes giving simple, safe and trustworthy. We are a one-stop shop for charitable giving and fundraising. We act as an e-meeting point for non-profit organizations, corporations, freelancers and citizens who want to support or be supported by the initiations we develop on our platform. In this section, please explore all the ways you can give through ChasteHelp!</p>
                </div>
            </div>
            <div class="grid">
                <div class="grid__item one-fifth text--center">
                    <img src="img/logo.png" alt="logo">
                    <p><em>Techliners.com</em></p>
                </div><!--
         --><div class="grid__item four-fifths">
                    <p>ChasteHelp makes giving simple, safe and trustworthy. We are a one-stop shop for charitable giving and fundraising. We act as an e-meeting point for non-profit organizations, corporations, freelancers and citizens who want to support or be supported by the initiations we develop on our platform. In this section, please explore all the ways you can give through ChasteHelp!</p>
                </div>
            </div>
        </div>
    </section>
    <section class="background background__image background__image--grid">
        <div class="container soft--top">
            <div class="grid">
                <div class="grid__item one-half">
                    <h4 class="font-base">JOB</h4>
                    <p>We have no job opening listed at the present</p>
                </div><!--
             --><div class="grid__item one-half">
                    <h4 class="font-base">CONTACT US</h4>
                    <form>
                        <ul class="form-fields">
                            <li class="clearfix">
                                <input class="text-input background background__grey two-fifths" placeholder="email">
                            </li>
                            <li>
                                <input class="text-input background background__grey two-fifths float--left" placeholder="Name">
                            </li>
                            <li>
                                <input class="text-input background background__grey two-fifths push--left" placeholder="Phone no.">
                            </li>
                            <li>
                                <textarea rows="4" cols="52" class="background background__grey " ></textarea>
                            </li>
                        </ul>

                        <button class="btn btn--hard background background__base text--center  push--top font-white font-cap">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </section>
</main>
<?php include 'global/footer.php' ?>
